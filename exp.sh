#!/bin/bash

#python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet50 --pointer 2 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
#wait
#python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet50 --pointer 3 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
#wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.001 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet50 --pointer 2 3 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
wait
#python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet50 --pointer 4 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
#wait
# python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss xent_loss --lambda0 0.001 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet50 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
# wait


python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet101 --pointer 2 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet101 --pointer 3 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.001 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet101 --pointer 2 3 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet101 --pointer 4 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
wait
# python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss xent_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet101 --balanced_classes 3 --batch-size 21 --dataset eggsmentations &
# wait





# If we have time I'm lunching on resnet152

python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.001 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet152 --pointer 2 3 --balanced_classes 3 --batch-size 18 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet152 --pointer 2 --balanced_classes 3 --batch-size 18 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet152 --pointer 3 --balanced_classes 3 --batch-size 18 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss DGMC_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet152 --pointer 4 --balanced_classes 3 --batch-size 18 --dataset eggsmentations &
wait
python3.8 N_run.py --path /home/super/tmp_datasets/CUB_200_2011/ --loss xent_loss --lambda0 0.01 --max-epoch 100 --lr 0.001 --save_model True --gpu 0,1 --model resnet152 --balanced_classes 3 --batch-size 18 --dataset eggsmentations &
wait