import datetime
import os
import os.path as osp
import time
import matplotlib
from matplotlib.lines import Line2D
#import seaborn as sns
from torch.optim.lr_scheduler import MultiStepLR
import utils
from DGMC_loss import DGMC_loss
from GMC_loss import GMC_loss
from utils import AverageMeter, compute_intravariance
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.multiprocessing as mp
import torch.nn as nn
from torch.optim import lr_scheduler
from center_loss import CenterLoss


def main(args, trainloader, testloader, num_classes, file_output):
        best_acc=0.
        #fixed colours for plot visualization 2D case.
        all_colours = num_classes
        colours = .6 * np.random.rand(all_colours, 3) + 0.4
        str_model = args.model
        dataset_name = args.path.split('/')[-2]
        if dataset_name == 'CUB_200_2011' or dataset_name == 'lego':
            from Resnet_large import Ext_ResNet
            model = Ext_ResNet(str_model, pointer=args.pointer, num_classes=num_classes)
            if model:
                print("Model loaded")
                dim_features = model.base_model.fc.in_features
            else:
                print("Loading models error")
                exit()
        # elif dataset_name == 'cifar100':
        #     if str_model == 'LLC_resnet18':
        #         from resnet_model import resnet18
        #         model = resnet18(str_model, args.graft, num_classes=num_classes)
        #     elif str_model == 'LLC_resnet50':
        #         from resnet_model import resnet50
        #         model = resnet50(str_model, args.graft, num_classes=num_classes)
        #     elif str_model == 'LLC_resnet101':
        #         from resnet_model import resnet101
        #         model = resnet101(str_model, args.graft, num_classes=num_classes)
        #     elif str_model == 'LLC_resnet152':
        #         from resnet_model import resnet152
        #         model = resnet152(str_model, args.graft, num_classes=num_classes)
        elif dataset_name == 'fgvc_dataset':
            from Resnet_large import Ext_ResNet
            model = Ext_ResNet(str_model, pointer=args.pointer, num_classes=num_classes)
            if model:
                print("Model loaded")
                dim_features = model.base_model.fc.in_features
                print(dim_features)
            else:
                print("Loading models error")
                exit()
        elif dataset_name == 'dogs_classification':
            from Resnet_large import Ext_ResNet
            model = Ext_ResNet(str_model, pointer=args.pointer, num_classes=num_classes)
            if model:
                print("Model loaded")
                dim_features = model.base_model.fc.in_features
                print(dim_features)
            else:
                print("Loading models error")
                exit()
        else:
            print("Error select dataset, please modify the path parameter")
            print("Dataset name extracted: ", dataset_name)
            exit()


        print("We have available ", torch.cuda.device_count(), "GPUs!")
        gpus_ids=[i for i in range(0,torch.cuda.device_count())]
        model = nn.DataParallel(model, device_ids=gpus_ids).cuda()

        criterion_xent = nn.CrossEntropyLoss()
        criterion_loss=None
        criterion_loss0 = None
        stretch_components = 1

        if args.loss == 'DGMC_loss':
            print("DGMC_loss set")
            print("Features dimension: ", dim_features)
            stretch_components1 = (np.linspace(10, 20, num=args.max_epoch, dtype=int, endpoint=False))
            #criterion_loss = GMC_loss(dim_hidden=dim_features, num_classes=num_classes,lambda_c=args.lambda0, stretch_components_second_last=)
            criterion_loss0 = DGMC_loss(num_classes=num_classes,lambda_c=args.lambda0, stretch_components_list=stretch_components1)


        #optimizer = torch.optim.Adam(model.parameters(), args.lr)
        optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=0.9, weight_decay=1e-4)
        #scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch)
        scheduler=torch.optim.lr_scheduler.StepLR(optimizer, step_size=30, gamma=0.1)


        start_time = time.time()
        for epoch in range(args.max_epoch):
            train_features, train_labels=train(trainloader, model, criterion_xent, criterion_loss, criterion_loss0, optimizer, num_classes, epoch, args, file_output,colours)
            #if epoch % 10 == 0:
            scheduler.step()
            #utils.adjust_learning_rate(optimizer, epoch, args)
            print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch)," Lr: ", utils.get_lr(optimizer), file=file_output)
            print("==> Test", file=file_output)
            acc, err = test(model, testloader, train_features, train_labels, args, file_output,num_classes, epoch, dim_features, stretch_components)
            print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err), file=file_output)

            if args.save_model and acc > best_acc:
                saved_model_path=osp.join(args.save_dir, 'model_w_' + args.model + 'dat_' + dataset_name + '_' + args.loss+ 'pointer_'+str(args.pointer))
                torch.save(model.state_dict(), saved_model_path)
                print("Model saved")
                best_acc=acc

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed), file=file_output)
        return True


def train(trainloader, model, criterion_xent, criterion_loss,criterion_loss0,
          optimizer, num_classes, epoch, args, file_output,colours):
    model.train()
    xent_losses = AverageMeter()
    GMC_losses = AverageMeter()
    DGMC_l1_losses, DGMC_l2_losses, DGMC_l3_losses, DGMC_l4_losses, \
    DGMC_l5_losses = AverageMeter(), AverageMeter(), AverageMeter(), AverageMeter(), AverageMeter()
    DGMC_l1_loss, DGMC_l2_loss, DGMC_l3_loss, DGMC_l4_loss = torch.tensor([0.]).cuda(), torch.tensor([0.]).cuda(),\
                                                             torch.tensor([0.]).cuda(), torch.tensor([0.]).cuda()
    acc_features, acc_labels = [],[]
    all_features, all_labels, all_paths = [], [], []
    for batch_idx, (data, labels) in enumerate(trainloader):
            data, labels = data.cuda(), labels.cuda()
            outputs, features, l1_distr, l2_distr, l3_distr, l4_distr = model(data)
            loss_xent = criterion_xent(outputs, labels)

            if args.loss=='DGMC_loss':
                #GMC_loss=criterion_loss(features, labels, epoch)
                if l1_distr !=None:
                    DGMC_l1_loss=criterion_loss0(l1_distr, labels, epoch)
                if l2_distr !=None:
                    DGMC_l2_loss=criterion_loss0(l2_distr, labels, epoch)
                if l3_distr !=None:
                    DGMC_l3_loss=criterion_loss0(l3_distr, labels, epoch)
                if l4_distr !=None:
                    DGMC_l4_loss=criterion_loss0(l4_distr, labels, epoch)

                loss = loss_xent + DGMC_l1_loss + DGMC_l2_loss + DGMC_l3_loss + DGMC_l4_loss

            elif args.loss == 'xent_loss':
                loss = loss_xent

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            xent_losses.update(loss_xent.item(), labels.size(0))
            all_features.append(features.data.cpu().numpy())
            all_labels.append(labels.data.cpu().numpy())

            if args.loss=='DGMC_loss':
                if l1_distr !=None:
                    DGMC_l1_losses.update(DGMC_l1_loss.item(), labels.size(0))
                if l2_distr !=None:
                    DGMC_l2_losses.update(DGMC_l2_loss.item(), labels.size(0))
                if l3_distr != None:
                    DGMC_l3_losses.update(DGMC_l3_loss.item(), labels.size(0))
                if l4_distr !=None:
                    DGMC_l4_losses.update(DGMC_l4_loss.item(), labels.size(0))
                #GMC_losses.update(GMC_loss.item(), labels.size(0))
                if (batch_idx + 1) % 10 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1, xent_losses.val,xent_losses.avg),
                                                                                                        file=file_output)
                    if l1_distr!=None:
                        print("DGMC_loss_l1 {:.6f} ({:.6f})".format(DGMC_l1_losses.val, DGMC_l1_losses.avg), file=file_output)
                    if l2_distr!=None:
                        print("DGMC_loss_l2 {:.6f} ({:.6f})".format(DGMC_l2_losses.val, DGMC_l2_losses.avg), file=file_output)
                    if l3_distr!=None:
                        print("DGMC_loss_l3 {:.6f} ({:.6f})".format(DGMC_l3_losses.val, DGMC_l3_losses.avg), file=file_output)
                    if l4_distr!=None:
                        print("DGMC_loss_l4 {:.6f} ({:.6f})".format(DGMC_l4_losses.val, DGMC_l4_losses.avg), file=file_output)
                    print("**********", file=file_output)

            if args.loss == 'xent_loss':
                if (batch_idx + 1) % 50 == 0:
                    print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1,xent_losses.val,xent_losses.avg),file=file_output)



    all_features = np.concatenate(all_features, 0)
    all_labels = np.concatenate(all_labels, 0)

    if (epoch + 1) % 1 == 0:
        #gcm = utils.Gaussian_cm(torch.cat(all_features), torch.cat(all_labels), num_classes)
        #utils.computer_covariance("Train", all_features, all_labels, num_classes, file_output)
        #voronoi_metric(all_features, all_labels, num_classes, file_output)
        #compute_intravariance("Train", torch.cat(all_features), torch.cat(all_labels), num_classes, file_output)
        plot_features(all_features, all_labels, num_classes, epoch, args, all_paths,criterion_loss,colours)
        pass

    return all_features, all_labels

def test(model, testloader, train_features, train_labels, args, file_output, num_classes, epoch, dim_features, stretch_components):
    model.eval()
    all_features, all_labels=[],[]
    correct, total = 0, 0
    confusion_matrix = np.zeros((num_classes, num_classes), dtype=int)
    with torch.no_grad():
        for idx, (data, labels) in enumerate(testloader):
                data, labels = data.cuda(), labels.cuda()
                outputs, features, _, _, _, _ = model(data)

                all_features.append(features.data.cpu().numpy())
                all_labels.append(labels.data.cpu().numpy())

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().float().item()

                for t, p in zip(predicted, labels):
                    confusion_matrix[int(t), int(p)] += 1


    test_features = np.concatenate(all_features, 0)
    test_labels = np.concatenate(all_labels, 0)
    #compute_intravariance("Test", torch.cat(test_features), torch.cat(test_labels), num_classes, file_output)
    #utils.compute_precision(confusion_matrix, file_output)
    #if (epoch + 1) % 100 == 0:
        #print("######################")
        #print("Bhatta analysis")
        #utils.bhatta(train_features, train_labels, test_features, test_labels, num_classes, epoch, file_output, dim_features, stretch_components, args)
    #if (epoch + 1) % 100 == 0:
    #    utils.density_plot(train_features, train_labels, test_features, test_labels, num_classes, epoch, args)
    acc = correct * 100. / total
    err = 100. - acc
    return acc, err


def plot_features(features, labels, num_classes, epoch, args, all_paths, criterion_loss, colours):
    plt.tick_params(labelsize=14)
    for label_idx in range(num_classes):
        plt.scatter(
            features[labels == label_idx, 0],
            features[labels == label_idx, 1],
            facecolor=colours[label_idx],
            s=2,
        )

    dirname = osp.join(args.save_dir, 'plot_gpu_'+args.gpu + args.path.split('/')[-2] + '_'+args.loss)
    if not osp.exists(dirname):
        os.mkdir(dirname)
    save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + '.png')
    plt.savefig(save_name, bbox_inches='tight', dpi=400)
    plt.close()


if __name__ == '__main__':
    main()
