import threading
import time
from queue import Queue
import torch.multiprocessing as mp
import torch
import numpy as np
import torch.nn as nn
import gmm
from gmm import GaussianMixture

def pairwise_distances(x, pivots):
    x_extended = torch.cat(pivots.shape[0] * [x])
    #y_extended = torch.cat((torch.cat(x.shape[0]* [a1]),torch.cat(x.shape[0] * [a2])),0)
    y_extended=tile(pivots, 0, x.shape[0])
    dist = torch.sum((x_extended - y_extended) ** 2, dim=1)
    dist = dist.reshape(pivots.shape[0], x.shape[0]).T
    top_val, top_idx = dist.topk(1, largest=False, dim=1)
    return torch.sum(top_val)

def pairwise_distances_2(x, pivots):
    #x_extended = pivots.shape[1] * [x]
    x_extended=x.repeat(1, pivots.shape[1], 1)
    #y_extended = torch.cat((torch.cat(x.shape[0]* [a1]),torch.cat(x.shape[0] * [a2])),0)
    #y_extended=tile(pivots, 0, x.shape[1])
    y_extended=pivots.repeat_interleave(x.shape[1], dim=1)
    dist = torch.sum((x_extended - y_extended) ** 2, dim=2)
    dist = dist.reshape(-1, x.shape[1],pivots.shape[1])
    top_val, top_idx = dist.topk(1, largest=False, dim=2)
    return torch.sum(top_val)




def tile(a, dim, n_tile):
    init_dim = a.size(dim)
    repeat_idx = [1] * a.dim()
    repeat_idx[dim] = n_tile
    a = a.repeat(*(repeat_idx))
    order_index = torch.LongTensor(np.concatenate([init_dim * np.arange(n_tile) + i for i in range(init_dim)])).cuda()
    return torch.index_select(a, dim, order_index)



class DGMC_loss(nn.Module):
    def __init__(self, num_classes, lambda_c, stretch_components_list):
        super(DGMC_loss, self).__init__()
        self.num_classes = num_classes
        self.lambda_c = lambda_c
        self.stretch_components_list= stretch_components_list

    def forward(self, hidden, y, epoch):
        selected_components=self.stretch_components_list[epoch]
        models =  [GaussianMixture(selected_components, hidden.shape[2]) for i in range(0,self.num_classes)]
        gaussian_center=[torch.zeros(selected_components, hidden.shape[2]).cuda() for i in range(0,self.num_classes)]
        class_instances = [hidden[(y == i)] for i in range(0, self.num_classes)]

        for i in range(0, len(class_instances)):
            if len(class_instances[i]) > 0:
                plane_instance_per_class_stacked=class_instances[i].contiguous().view(-1, class_instances[i].shape[2])
                models[i].fit(plane_instance_per_class_stacked, n_iter=100)
                gaussian_center[i]=models[i].mu.reshape(selected_components, hidden.shape[2])

        # for i in range(0, hidden.shape[0]):
        #     thread=threading.Thread(target=models[i].fit, args=(hidden[i], 150))
        #     thread.start()
        #     t.append(thread)
        #
        #
        # for i in range(0, hidden.shape[0]):
        #     t[i].join()


        # for i in range(0, hidden.shape[0]):
        #     gaussian_center.append(models[i].mu.reshape(selected_components, hidden.shape[2]))

        gaussian_center_stacked=torch.stack(gaussian_center)

        # intra = pairwise_distances_2(hidden, gaussian_center_stacked)
        # print("Time: ", time.time() - start)
        # loss = (0.001/hidden.shape[0]) * intra
        # return loss

        intra = 0.
        for i in range(0, len(class_instances)):
            if len(class_instances[i]) > 0:
                plane_instance_per_class_stacked=class_instances[i].contiguous().view(-1, class_instances[i].shape[2])
                c = pairwise_distances(plane_instance_per_class_stacked, gaussian_center_stacked[i])
                intra += c

        #print("Time: ", time.time() - start)
        loss = (self.lambda_c / (hidden.shape[0]*hidden.shape[1])) * intra
        return loss

