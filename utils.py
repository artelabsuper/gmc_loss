import os
import sys
import errno
import shutil
import os.path as osp
import numpy as np
import itertools
import torch
from matplotlib.lines import Line2D
from matplotlib import pyplot as plt
#import seaborn as sns

is_torchvision_installed = True
try:
    import torchvision
except:
    is_torchvision_installed = False

import torch.utils.data
import random

def centeroidnp(arr):
    length = arr.shape[0]
    return torch.sum(arr, 0)/length

def std_2d(a):
    x_mean=centeroidnp(a)
    var=torch.sum(torch.sqrt(torch.sum((a - x_mean)**2, 1)))/(a.shape[0])
    return var.item()

def mkdir_if_missing(directory):
    if not osp.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise


class AverageMeter(object):
    """Computes and stores the average and current value.

       Code imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def save_checkpoint(state, is_best, fpath='checkpoint.pth.tar'):
    mkdir_if_missing(osp.dirname(fpath))
    torch.save(state, fpath)
    if is_best:
        shutil.copy(fpath, osp.join(osp.dirname(fpath), 'best_model.pth.tar'))


class Logger(object):
    def __init__(self, fpath=None):
        self.file = None
        self.terminal = sys.stdout
        if fpath is not None:
            mkdir_if_missing(os.path.dirname(fpath))
            self.file = open(fpath, 'w')

    def write(self, msg):
        self.terminal.write(msg)
        if self.file is not None:
            self.file.write(msg)

    def flush(self):
        if self.file is not None:
            self.file.flush()
            os.fsync(self.file.fileno())

    def close(self):
        if self.file is not None:
            self.file.close()


def _one_pass(iters):
    for it in iters:
        try:
            yield next(it)
        except StopIteration:
            pass  # of some of them are already exhausted then ignore it.


def adjust_learning_rate(optimizer, epoch, args):
    if epoch == 149:
        lr_model = args.lr * 0.1
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr_model

    elif epoch == 224:
        lr_model = get_lr(optimizer) * 0.1
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr_model


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


from gmm import GaussianMixture


def bhatta_dist(X1, X2, dim_features, stretch_components, epoch, args, method='continuous'):

    def fit(x0, dim_features):
        if args.loss == 'center_loss' or args.loss=='xent_loss':
            model0 = GaussianMixture(2, dim_features)
        else:
            model0 = GaussianMixture(2, dim_features)

        model0.fit(x0, n_iter=600)
        return model0


    cX = torch.cat((X1,X2))
    max_xc = torch.max(cX)
    min_xc = torch.min(cX)


    if method == 'continuous':
        N_STEPS = X2.shape[0]
        d1 = fit(X1, dim_features)
        p1 = d1.predict_proba(X2)
        p1 = torch.where(torch.isnan(p1), torch.zeros_like(p1), p1)
        #bht = torch.sum(p1/N_STEPS).float()
        bht=torch.sum(torch.sqrt(p1)) * (max_xc - min_xc) / N_STEPS


    else:
        raise ValueError("The value of the 'method' parameter does not match any known method")

    ###Lastly, convert the coefficient into distance:
    print("BC: ",bht)
    if bht==0:
        return float('Inf')
    else:
        return -torch.log(bht)



def bhatta(train_features, train_labels, test_features, test_labels, num_classes, epoch, file_output, dim_features,stretch_components, args):
    class_train_instances = [train_features[(train_labels == i)] for i in range(0, num_classes)]
    class_test_instances = [test_features[(test_labels == i)] for i in range(0, num_classes)]

    print("Epoch: ", epoch)
    for idx, (p, q) in enumerate(zip(class_train_instances, class_test_instances)):
        print("Class: ", idx, file=file_output)
        print("Bhatta distance {0:.8f}".format(bhatta_dist(torch.from_numpy(p).cuda(), torch.from_numpy(q).cuda(), dim_features, stretch_components, epoch, args, method='continuous')), file=file_output)
        print("******************************")


def density_plot(train_features, train_labels, test_features, test_labels, num_classes, epoch, args):
    dirname = osp.join(args.save_dir, 'Density_plot_gpu_' + args.gpu + args.path.split('/')[-2] + '_' + args.loss)
    if not osp.exists(dirname):
        os.mkdir(dirname)

    class_train_instances = [train_features[(train_labels == i)] for i in range(0, num_classes)]

    class_test_instances = [test_features[(test_labels == i)] for i in range(0, num_classes)]


    for i in range(len(class_train_instances)):
        sns.kdeplot(class_train_instances[i][:, 0], class_train_instances[i][:, 1], cmap="YlOrRd",  # YlGnBu
                    shade=True,
                    shade_lowest=True,
                    n_levels=40,
                    antialiased=True)
        sns.scatterplot(class_test_instances[i][:, 0], class_test_instances[i][:, 1], marker="x", color="b", alpha=0.3, size=1)
        save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + " Class: " + str(i) + '.png')
        custom = [Line2D([], [], marker='.', color='r', linestyle='None'),
                  Line2D([], [], marker='.', color='b', linestyle='None')]
        plt.legend(custom, ['Train Feat', 'Test Feat'], loc='upper right')
        plt.savefig(save_name, bbox_inches='tight', dpi=400)
        plt.close()



def compute_precision(cm, file_output):
    print("*****************************************", file=file_output)
    for z in range(cm.shape[0]):
        print("Precision class: ",z, " ->",cm[z][z]/cm[z].sum(), file=file_output)
    print("*****************************************", file=file_output)

# def compute_precision_from_gcm(cm, file_output):
#     for z in range(cm.shape[0]):
#         print("Precision class: ",z, " ->",cm[z][z]/cm[z].sum(), file=file_output)
#     print("*****************************************", file=file_output)



# def Gaussian_cm(feat,labels, num_classes):
#     models = [GaussianMixture(2, 64) for i in range(0, num_classes)]
#     class_instances = [feat[(labels == i)] for i in range(0, num_classes)]
#     #max=np.max([len(i) for i in class_instances])
#     confusion_matrix = np.zeros((num_classes, num_classes), dtype=int)
#
#
#     for i in range(0, num_classes):
#             models[i].fit(class_instances[i], n_iter=150)
#
#     for i in range(0, num_classes):
#         prd = []
#         for j in range(0, num_classes):
#             z,_=torch.max(models[j].predict(class_instances[i], probs=True),1)
#             prd.append(z.cuda())
#
#
#         prd_stacked=torch.stack(prd)
#         prd_stacked[torch.isnan(prd_stacked)] = torch.tensor([0.]).cuda()
#
#         prd_values, prd_idx =torch.max(prd_stacked, 0)
#         prd_idx=prd_idx.data.cpu().numpy()
#         prd_values=prd_values.data.cpu().numpy()
#
#         for p_v, p_i in zip(prd_values,prd_idx):
#                 confusion_matrix[i, p_i] += 1
#
#
#     return confusion_matrix



def zip_varlen(*iterables):
    iters = [iter(it) for it in iterables]
    while True:  # broken when an empty tuple is given by _one_pass
        val = tuple(_one_pass(iters))
        if val:
            yield val
        else:
            break


def compute_intravariance(which,data,labels, num_classes, file_output):
    tmp=which+" Intravariance: "
    print(tmp, file=file_output)
    class_instances = [data[(labels == i)] for i in range(0, num_classes)]
    for i in range(len(class_instances)):
        print(which+" Class ",i, ": ", len(class_instances[i]), "Std: ", std_2d(class_instances[i]), file=file_output)


def computer_covariance(which, data, labels, num_classes, file_output):
    tmp = which + " Covariance: "
    print(tmp, file=file_output)
    class_instances = [data[(labels == i)] for i in range(0, num_classes)]
    for idx, i in enumerate(class_instances):
        print("Class ", idx, ": ", len(i),"Covariance: ", np.cov(data.T)[0][0], file=file_output)



from typing import Iterator, Optional, List, TypeVar, Generic, Sized

T_co = TypeVar('T_co', covariant=True)

class Sampler(Generic[T_co]):
    r"""Base class for all Samplers.

    Every Sampler subclass has to provide an :meth:`__iter__` method, providing a
    way to iterate over indices of dataset elements, and a :meth:`__len__` method
    that returns the length of the returned iterators.

    .. note:: The :meth:`__len__` method isn't strictly required by
              :class:`~torch.utils.data.DataLoader`, but is expected in any
              calculation involving the length of a :class:`~torch.utils.data.DataLoader`.
    """

    def __init__(self, data_source: Optional[Sized]) -> None:
        pass

    def __iter__(self) -> Iterator[T_co]:
        raise NotImplementedError

    # NOTE [ Lack of Default `__len__` in Python Abstract Base Classes ]
    #
    # Many times we have an abstract class representing a collection/iterable of
    # data, e.g., `torch.utils.data.Sampler`, with its subclasses optionally
    # implementing a `__len__` method. In such cases, we must make sure to not
    # provide a default implementation, because both straightforward default
    # implementations have their issues:
    #
    #   + `return NotImplemented`:
    #     Calling `len(subclass_instance)` raises:
    #       TypeError: 'NotImplementedType' object cannot be interpreted as an integer
    #
    #   + `raise NotImplementedError()`:
    #     This prevents triggering some fallback behavior. E.g., the built-in
    #     `list(X)` tries to call `len(X)` first, and executes a different code
    #     path if the method is not found or `NotImplemented` is returned, while
    #     raising an `NotImplementedError` will propagate and and make the call
    #     fail where it could have use `__iter__` to complete the call.
    #
    # Thus, the only two sensible things to do are
    #
    #   + **not** provide a default `__len__`.
    #
    #   + raise a `TypeError` instead, which is what Python uses when users call
    #     a method that is not defined on an object.
    #     (@ssnl verifies that this works on at least Python 3.7.)


class PartialBalancedBatchSampler(torch.utils.data.sampler.Sampler):
    r"""Samples elements randomly balanced per class for each batch until to achieve the batch-size.
        When it achieve the batch size, the lenght of others classes will be 0.
        If it is not possibile to take n balanced_classes from a class because the class has less elements, it takes
        all last elements.
        If it is not possibile to take n_balanced_classes because the batch-size is full, the remaining
        elements will be take the next batch.

        Args:
            dataset (Dataset): dataset to sample from
            balanced_classes (int): number of random samples in a generic class for each batch.
            num_classes (int): number of classes
    """

    def __init__(self, dataset, balanced_classes, num_classes):
        self.dataset = dict()
        self.d = dataset
        self.balanced_classes = balanced_classes
        self.num_cl_list = list(range(num_classes))
        self.train_idx = list(range(len(dataset)))


    def __iter__(self):
        for idx in self.train_idx:
            label = self._get_label(self.d, idx)
            if label not in self.dataset:
                self.dataset[label] = []
            self.dataset[label].append(idx)

        len_classes_dataset = [len(i) for i in self.dataset.values()]
        if any(self.balanced_classes > i for i in len_classes_dataset):
            raise Exception('Number of balanced classes should be less than ->' + str(min(len_classes_dataset)))
        rand_tensor = []
        while any(self.dataset.values()):
            random.shuffle(self.num_cl_list)
            for i in self.num_cl_list:
                stack_class = len(self.dataset[i])
                if self.balanced_classes < stack_class:
                    r_values = random.sample(self.dataset[i], k=self.balanced_classes)
                    rand_tensor.append(r_values)
                    for j in r_values:
                        self.dataset[i].remove(j)
                else:
                    r_values = random.sample(self.dataset[i], k=stack_class)
                    rand_tensor.append(r_values)
                    for j in r_values:
                        self.dataset[i].remove(j)

        flatten = list(itertools.chain(*rand_tensor))
        return iter(flatten)


    def _get_label(self, dataset, idx):
        return dataset.imgs[idx][1]

    # def _get_label(self, dataset, idx):
    #         dataset_type = type(dataset)
    #         if isinstance(dataset, torchvision.datasets.ImageFolder):
    #             return dataset.imgs[idx][1]
    #         elif dataset_type is Albumentations_loader.AlbuDataLoader:
    #             return dataset[idx][1]
    #         else:
    #             raise Exception("You should pass the tensor of labels to the constructor as second argument")
    #             exit()
