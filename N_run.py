import os
import os.path as osp
import torch
from torch.backends import cudnn
import base_main
import argparse
import datasets_semplified
from utils import Logger


def args_parser():
    parser = argparse.ArgumentParser("Loss Example")
    parser.add_argument('-d', '--dataset', type=str)
    parser.add_argument('--batch-size', type=int, default=128)
    parser.add_argument('--lr', type=float, default=0.01, help="learning rate for model")
    parser.add_argument('--lr_cent', type=float, default=0.01, help="learning rate for center")
    parser.add_argument('--max-epoch', type=int, default=500)
    parser.add_argument('--balanced_classes', type=int, default=100)
    parser.add_argument('--save_model', type=str, default='False', help="save model")
    parser.add_argument('--gpu', type=str, default='0')
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--save-dir', type=str, default='log')
    parser.add_argument('--path', type=str, default='dataset')
    parser.add_argument('--plot', action='store_true', help="whether to plot features for every epoch")
    parser.add_argument('--loss', type=str)
    parser.add_argument('--pretrained', type=str, default='True', help="load pretrained model")
    parser.add_argument('--lambda0', type=float, default=1.)
    parser.add_argument('--pointer', type=int, nargs='+', default=[-1])
    parser.add_argument('--model', type=str, default='resnet50')
    return parser.parse_args()


n_exp=1
args=args_parser()
torch.manual_seed(args.seed)
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

use_gpu = torch.cuda.is_available()
print("Let's use", torch.cuda.device_count(), "GPUs!")
str_dataset=args.path.split('/')[-2]
# if args.dataset=='MyDataset_cross_val':
#     print("Cross-validation set..")
#     kf = 5
#     for idx_k in range(kf):
#         dataset = datasets_semplified.create(
#             name=args.dataset, batch_size=args.batch_size, path=args.path, balanced_classes=args.balanced_classes
#         )
#         trainloader, testloader, num_classes = dataset.trainloader, dataset.testloader, dataset.num_classes
#         if args.loss == 'xent_loss':
#             file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss))
#         elif args.loss == 'center_loss':
#             file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(args.lambda0)+'_lr_cent='+str(args.lr_cent)))
#         elif args.loss == 'GMC_loss':
#             file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(
#                 args.lambda0)))
#         if use_gpu:
#             print(
#                 "Lr {:}  balanced_classes {:} Batch-size {:}  Loss = {:} Lambda = {:}".format(
#                     args.lr, args.balanced_classes, args.batch_size, args.loss, args.lambda0, ), file=file_output)
#             cudnn.benchmark = True
#             torch.cuda.manual_seed_all(args.seed)
#         else:
#             print("Currently using CPU, abort()", file=file_output)
#             exit()
#         if base_main.main(args, trainloader, testloader, num_classes, file_output):
#             print("Success", file=file_output)
#             file_output.close()
#         else:
#             print("Abort", file=file_output)
#             exit()
if args.dataset=='eggsmentations':
    kf = 1
    print("Training repeated ",kf  ," time with data augmentation")
    for idx_k in range(kf):
        dataset = datasets_semplified.create(name=args.dataset, batch_size=args.batch_size, path=args.path, balanced_classes=args.balanced_classes)
        trainloader, testloader, num_classes = dataset.trainloader, dataset.testloader, dataset.num_classes
        print("K-FOLD: ", idx_k)
        if args.loss == 'xent_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_model_'+ args.model + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss +'.txt'))
        elif args.loss == 'center_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(args.lambda0)+'_lr_cent='+str(args.lr_cent)+'.txt'))
        elif args.loss == 'DGMC_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_model_'+ args.model + '_log_' + str_dataset + '_gpu= ' + args.gpu + '_loss=' + args.loss + '_lambda=' + str(args.lambda0)+'_pointer_'+str(args.pointer) +'.txt'))
        if use_gpu:
            print("Lr {:}  balanced_classes {:} Batch-size {:}  Loss = {:} Lambda = {:}  pointer = {:}  model = {:}".format(args.lr, args.balanced_classes, args.batch_size,args.loss,args.lambda0, args.pointer, args.model),file=file_output)
            cudnn.benchmark = True
            cudnn.enabled = True
        else:
            print("Currently using CPU, abort()", file=file_output)
            exit()
        if base_main.main(args, trainloader, testloader, num_classes, file_output):
            print("Success", file=file_output)
            file_output.close()
        else:
            print("Abort", file=file_output)
            exit()