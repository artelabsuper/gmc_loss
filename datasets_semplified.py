from PIL import Image
from torchvision import datasets
from torch.utils.data import DataLoader
import torch
import albumentations as albu
import numpy
import torchvision
from utils import PartialBalancedBatchSampler
from torchvision.transforms import transforms



def get_training_augmentation():
    train_transform = [
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     #albu.ShiftScaleRotate(p=0.5),
     #albu.IAAPerspective(p=0.5),
    albu.OneOf([
        #albu.VerticalFlip(),
        albu.HorizontalFlip(),
    ], p=0.5),

     albu.Rotate(p=0.5),
     albu.RandomResizedCrop(32,32, scale=(0.7, 1.0),p=0.5),
     #albu.RandomBrightnessContrast(p=0.3),
     #albu.CLAHE(p=0.3),
     #albu.Blur(p=0.3)
     #albu.ChannelShuffle(p=0.5),
     #albu.RandomGamma(p=0.5),
     #albu.RandomGridShuffle(grid=(3, 3), p=0.5),
     #albu.RandomRotate90(p=0.5),
    ]
    return albu.Compose(train_transform,p=0.5)



class eggsmentations(object):
    def __init__(self, batch_size, path, balanced_classes):
        self.batch_size = batch_size
        self.path=path
        self.balanced_classes=balanced_classes

        self.dataset_name = path.split('/')[-2]
        print("Dataset selected: ")

        if self.dataset_name == 'CUB_200_2011':
            print(self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((600, 600)),
                torchvision.transforms.RandomCrop((448,448)),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((600, 600)),
                torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

        if self.dataset_name == 'lego':
            print(self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((64, 64)),
                #torchvision.transforms.RandomCrop((448,448)),
                #torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                #torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((64, 64)),
                #torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                #torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])


        elif self.dataset_name == 'cifar100' or self.dataset_name == 'cifar10':
            train_transforms = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
            ])

            # Normalize test set same as training set without augmentation
            test_transforms = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
            ])
        elif self.dataset_name == 'fgvc_dataset':
            # rate=0.875
            print("Aircraft ", self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((448, 448), Image.BILINEAR),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ColorJitter(brightness=0.2, contrast=0.2),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize((448, 448), Image.BILINEAR),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])
        elif self.dataset_name == 'dogs_classification':
            print(self.dataset_name)
            train_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize([512, 512]),
                torchvision.transforms.RandomCrop([448, 448]),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

            test_transforms = torchvision.transforms.Compose([
                torchvision.transforms.Resize([512, 512]),
                torchvision.transforms.CenterCrop(448),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

        train_dataset = datasets.ImageFolder(self.path + '/train', transform=train_transforms)

        # train_loader = torch.utils.data.DataLoader(train_dataset,sampler=PartialBalancedBatchSampler(train_dataset,
        #                                                                                 balanced_classes=balanced_classes,
        #                                                                                 num_classes=len(train_dataset.classes)),
        #                                            batch_size=batch_size, shuffle=False, num_workers=4,pin_memory=True)

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=4, pin_memory=True)

        test_dataset = datasets.ImageFolder(self.path + '/test', transform=test_transforms)
        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=4,pin_memory=True)

        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset), "Classes: ", self.num_classes, "Test loader set: ",
              len(test_dataset.samples))
        # for data,label in train_loader:
        #     class_instances = [data[(label == i)] for i in range(0, self.num_classes)]
        #     print([len(i) for i in class_instances])
        # exit()





class AlbumentationToTorchvision:
    def __init__(self, compose):
        self.compose = compose

    def __call__(self, x):
        data = {"image": x}
        return self.compose(**data)['image']


class ToNumpy:
    def __call__(self, x):
        return numpy.array(x)




__factory = {
    'eggsmentations': eggsmentations
}


def create(name, batch_size, path, balanced_classes):
    if name not in __factory.keys():
        raise KeyError("Unknown dataset: {}".format(name))
    return __factory[name](batch_size, path, balanced_classes)
